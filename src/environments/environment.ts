// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "8e3638820945bcc7b9fdac98f66a7142a462ad13",
		authDomain: "mysafetynet-f1902.firebaseapp.com",
		databaseURL: "https://mysafetynet-f1902.firebaseio.com/",
		projectId: "mysafetynet-f1902",
		storageBucket: "lfk-parent.appspot.com",
		messagingSenderId: "859740767333"
  }
};