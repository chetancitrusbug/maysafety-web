import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from '../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { EditChildValidation } from '../../service/validation/edit-child-validation';
import { childeditprofile } from "./../../app-data";
import { config } from "../../app-data";

@Component({
  selector: 'app-editprofile-component',
  templateUrl: './editprofile-component.component.html',
  styleUrls: ['./editprofile-component.component.css']
})
export class EditprofileComponentComponent implements OnInit {

  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  childForm: FormGroup;
  formBuilder: FormBuilder;
  submitted = false;
  formload = false;
  previewImg : any= "";

  public item: any = {};
  public pagedata: any = childeditprofile;
  constructor(formBuilder: FormBuilder, public api: AuthApiService, private subscription: CommonEventsService) {
    this.formBuilder = formBuilder;
    this.item = JSON.parse(localStorage.getItem("authuser"));
    this.createForm(this.item);
  }

  ngOnInit() {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    console.log(authuser);
    this.item = authuser;
    this.getProfile(authuser.id);
  }

  createForm(item: any) {
    this.childForm = this.formBuilder.group({
      'child_id': [item.id, Validators.required],
      'first_name': [item.first_name, Validators.required],
      'last_name': [item.last_name, Validators.required],
      'phone': [item.phone, Validators.required],
      'school_name': [item.school_name, Validators.required],
      'school_district_no': [item.school_district_no, Validators.required],
      'state': [item.state, Validators.required],
      'image': null
    },{'validator': EditChildValidation.Formvalidation});
  }
  getProfile(child_id: any) {
    this.api.getAuthData("child/childDetail?child_id=" + child_id, '').subscribe(result => {
      this.item = result.result;
      this.createForm(this.item);
    },
      (error) => { this.formload = false; })
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.childForm.get('image').setValue(file);

      var reader = new FileReader();
      
      reader.onload = (event) => {
        this.previewImg = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
    } else {
      this.childForm.get('image').setValue(null);
      this.previewImg = "";
    }
  }

  private prepareSave(): any {
    
    let input = new FormData();
    input.append('child_id', this.childForm.get('child_id').value);
    input.append('first_name', this.childForm.get('first_name').value);
    input.append('last_name', this.childForm.get('last_name').value);
    input.append('school_name', this.childForm.get('school_name').value);
    input.append('school_district_no', this.childForm.get('school_district_no').value);
    input.append('state', this.childForm.get('state').value);

    if (this.childForm.get('image').value) {
      input.append('image', this.childForm.get('image').value);
    }
    if (this.childForm.get('phone').value) {
      let aphone = this.childForm.get('phone').value;
      input.append('phone', aphone.replace(/[^0-9]/g, ''));
    }

    return input;
  }
  onSubmit(formData: any) {
    this.submitted = true;
    if (this.childForm.invalid) {
      return;
    } else {
      this.formload = true;
      let formvalue = this.prepareSave();
      this.upload("child/register/edit", formvalue);

    }
  }



  public upload(modal: string, myData: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          this.enableForm();
          if (xhr.status === 200) {
            let authuser: any = JSON.parse(localStorage.getItem("authuser"));
            let res = JSON.parse(xhr.response).result;
            authuser.first_name = res.first_name;
            authuser.last_name = res.last_name;
            authuser.phone = res.phone;
            authuser.school_name = res.school_name;
            authuser.school_district_no = res.school_district_no;
            authuser.state = res.state;
            authuser.image = res.image;
            authuser.thumb_image = res.thumb_image;
            this.previewImg = "";
            
            localStorage.setItem('authuser',JSON.stringify(authuser));
            this.displaySuccess(JSON.parse(xhr.response));
            resolve(JSON.parse(xhr.response));
          } else {
            this.displayError(JSON.parse(xhr.response));
            reject(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        //this.progress = Math.round(event.loaded / event.total * 100);
        //this.progressObserver.next(this.progress);
      };

      let token = "";
      let authuser: any = JSON.parse(localStorage.getItem("authuser"));
      if (authuser && authuser.token != '') {
        token = authuser.token;
      }
      xhr.open('POST', config.api_url + modal, true);
      xhr.setRequestHeader('Authorization', 'bearer ' + token);
      xhr.send(myData);
    });
  }

  displaySuccess(result: any) {
    let sucsess_ob = { action: 'flash_success', redirect_to: 'child', dataobj: { 'message': result.message, 'message_head': 'Success !' } };
    this.subscription.globleEvent(sucsess_ob);
  }
  displayError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if (error && error.message) {
      errMsg = error.message;
    }
    let error_ob = { action: 'flash_error', redirect_to: '', dataobj: { 'message': errMsg, 'message_head': 'Request Not Procceed !' } };
    this.subscription.globleEvent(error_ob);
  }

  enableForm(){
    let that = this;
    setTimeout(function(){
      that.formload = false;
    },2000);
  }

}
