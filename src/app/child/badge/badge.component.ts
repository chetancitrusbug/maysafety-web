import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from '../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';  

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.css']
})
export class BadgeComponent implements OnInit {

  passForm : FormGroup;
  submitted = false;
  formload = false;

  public profile: any = {};

  constructor(formBuilder: FormBuilder, public api: AuthApiService,private subscription: CommonEventsService) { 
    
    
  }

  ngOnInit() {
  }
}
