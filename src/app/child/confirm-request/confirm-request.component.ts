import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from './../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';
import { confirmrquest } from "./../../app-data";

@Component({
  selector: 'app-confirm-request',
  templateUrl: './confirm-request.component.html',
  styleUrls: ['./confirm-request.component.css']
})
export class ConfirmRequestComponent implements OnInit {

  submitted = false;
  profileForm: FormGroup;
  formload_a = false;
  formload_b = false;
  public pagedata: any = confirmrquest;

  public profile: any = {};
  public first_requst: any = {};
  public first_requst_user: any = {};
  public notification: any = [];

  constructor(private route: ActivatedRoute, private router: Router, public api: AuthApiService, private subscription: CommonEventsService) {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    this.profile = authuser;

  }

  ngOnInit() {
    this.getMyNotification("");
  }

  getMyNotification(myData: any) {
    this.api.getAuthData("child/get_notification?type=Request", myData).subscribe(result => {
      this.notification = result.result;
      this.getFirstRequest();
    },
      (error) => { })
  }
  getFirstRequest() {
    if (this.notification && this.notification.length > 0 && this.profile) {
      this.first_requst = this.notification[0];
      if (this.profile.id == this.first_requst.child_a_id) {
        this.first_requst_user = this.first_requst.receiver;
      } else {
        this.first_requst_user = this.first_requst.sender;
      }
    }
  }
  approve(batch_id: any, id: number) {
    this.formload_a = true;
    let formData = { batch_id: batch_id, device_type: '' };
    this.api.submitAuth("child/send_notification", formData).subscribe(result => {
      this.enableForm('formload_a');
      this.notification = this.notification.filter(item => item.id !== id);
      this.getFirstRequest();
     // this.markasread(batch_id, id);
      let sucsess_ob = { action: 'flash_success', redirect_to: '', dataobj: { 'message': result.message, 'message_head': 'Success !' } };
      this.subscription.globleEvent(sucsess_ob);
    },
      (error) => { this.enableForm('formload_a'); this.displayError(error); })
  }
  markasread(batch_id: any, id: number) {
    let formData = { notification_id: id, read_flag: 1 };
    this.api.submitAuth("child/update_notification", formData).subscribe(result => {
      this.subscription.globleEvent({ action: 'load_notification', redirect_to: '', dataobj: { 'message': '', 'message_head': '' } });
    }, (error) => { })
  }
  reject(batch_id: any, id: number) {

    this.formload_b = true;
    let formData = { notification_id: id, read_flag: 1 };
    this.api.submitAuth("child/update_notification", formData).subscribe(result => {
      this.enableForm('formload_b');
      this.notification = this.notification.filter(item => item.id !== id);
      this.getFirstRequest();
      let sucsess_ob = { action: 'flash_success', redirect_to: '', dataobj: { 'message': result.message, 'message_head': 'Success !' } };
      this.subscription.globleEvent(sucsess_ob);
      this.subscription.globleEvent({ action: 'load_notification', redirect_to: '', dataobj: { 'message': '', 'message_head': '' } });
    },
      (error) => { this.enableForm('formload_b'); this.displayError(error); })


  }



  displayError(error: any) {

    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if (error.error && error.error.message) {
      errMsg = error.error.message
    }
    let error_ob = { action: 'flash_error', redirect_to: '', dataobj: { 'message': errMsg, 'message_head': 'Request Not Procceed !' } };
    this.subscription.globleEvent(error_ob);
  }
  enableForm(formid:string) {
    let that = this;
    setTimeout(function () {
      if(formid == "formload_b"){
        that.formload_b = false;
      }else{
        that.formload_a = false;
      }
      
    }, 2000);
  }
}
