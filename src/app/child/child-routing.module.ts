import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { BadgeComponent } from './badge/badge.component';
import { ChildChangepasswordComponent } from './child-changepassword/child-changepassword.component';
import { ConfirmRequestComponent } from './confirm-request/confirm-request.component';
import { RequestComponent } from './request/request.component';
import { ProfileComponentComponent } from './profile-component/profile-component.component';
import { EditprofileComponentComponent } from './editprofile-component/editprofile-component.component';
import { NotificationComponentComponent } from './notification-component/notification-component.component';

const routes: Routes = [
  {
      path: '',
      component: ProfileComponentComponent
  },
 /* {
    path: 'edit-profile',
    component: EditprofileComponentComponent
  },*/
  {
    path: 'badge1',
    component: BadgeComponent
  },
  {
    path: 'request',
    component: ConfirmRequestComponent
  },
  {
    path: 'notification',
    component: NotificationComponentComponent
  },
  {
    path: 'badge',
    component: RequestComponent
  },
  {
    path: 'change-password',
    component: ChildChangepasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChildRoutingModule { }
