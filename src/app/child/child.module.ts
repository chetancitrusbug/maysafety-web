import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TextMaskModule } from 'angular2-text-mask';

import { ChildRoutingModule } from './child-routing.module';
import { BadgeComponent } from './badge/badge.component';
import { ChildChangepasswordComponent } from './child-changepassword/child-changepassword.component';
import { ConfirmRequestComponent } from './confirm-request/confirm-request.component';
import { RequestComponent } from './request/request.component';
import { ProfileComponentComponent } from './profile-component/profile-component.component';
import { EditprofileComponentComponent } from './editprofile-component/editprofile-component.component';
import { NotificationComponentComponent } from './notification-component/notification-component.component';


@NgModule({
  imports: [
    CommonModule,
    ChildRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule
  ],
  declarations: [
    BadgeComponent,
    ChildChangepasswordComponent,
    ConfirmRequestComponent,
    RequestComponent,
    ProfileComponentComponent,
    EditprofileComponentComponent,
    NotificationComponentComponent
  ]
})
export class ChildModule { }
