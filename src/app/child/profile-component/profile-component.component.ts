import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from '../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';  
import { childprofile } from "./../../app-data";

@Component({
  selector: 'app-profile-component',
  templateUrl: './profile-component.component.html',
  styleUrls: ['./profile-component.component.css']
})
export class ProfileComponentComponent implements OnInit {

  passForm : FormGroup;
  submitted = false;
  formload = false;

  public profile: any = {};
  public pagedata: any = childprofile;

  constructor(formBuilder: FormBuilder, public api: AuthApiService,private subscription: CommonEventsService) { 
    
  }
  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

  ngOnInit() {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    console.log(authuser);
    this.profile = authuser;
    this.getProfile(authuser.id);
  }

  getProfile(child_id:any) {
    
    
    this.formload = true;
    this.api.getAuthData("child/childDetail?child_id="+child_id,'').subscribe(result => {
      this.formload = false;
      this.profile = result.result;
    },
    (error) => { this.formload = false; })
}

}
