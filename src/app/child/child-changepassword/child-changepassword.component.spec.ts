import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildChangepasswordComponent } from './child-changepassword.component';

describe('ChildChangepasswordComponent', () => {
  let component: ChildChangepasswordComponent;
  let fixture: ComponentFixture<ChildChangepasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildChangepasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildChangepasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
