import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from './../../default/auth-api.service';  
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';  
import { badge } from "./../../app-data";

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  submitted = false;
  badgeForm : FormGroup;
  formload = false;

  public pagedata: any = badge;
  public profile: any = {};

  constructor(formBuilder: FormBuilder,private route: ActivatedRoute, private router: Router,public api: AuthApiService,private subscription: CommonEventsService) {
    this.badgeForm = formBuilder.group({
      'batch_id' : [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  onSubmit(formData:any) {
    
    this.submitted = true;
    
    if (this.badgeForm.invalid) {
      return;
   }else{
      this.formload = true;
      this.api.submitAuth("child/view_batch_child",formData).subscribe(result => {
        this.enableForm();
        let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':result.result.message,'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
        
      },
      (error) => {this.displayError(error);})
   }
  }
  
  displayError(error:any) {
    this.enableForm();
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  
    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
  }
  enableForm(){
    let that = this;
    setTimeout(function(){
      that.formload = false;
    },2000);
  }


}
