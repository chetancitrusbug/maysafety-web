import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { mychildlist } from "./../../app-data";
import { AuthApiService } from './../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';

@Component({
  selector: 'app-mychild-component',
  templateUrl: './mychild-component.component.html',
  styleUrls: ['./mychild-component.component.css']
})
export class MychildComponentComponent implements OnInit {

  submitted = false;
  formload = false;
  process_username = "";

  public childlist: any[] = [];
  public userdetail: any = {};
  public pagedata: any = mychildlist;
  

  constructor(formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, public api: AuthApiService, private subscription: CommonEventsService) {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));

  }

  ngOnInit() {
    this.getchilds("");
  }
  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

  getchilds(myData: any) {
    this.formload = true;
    this.api.getAuthData("parent/view_child", myData).subscribe(result => {
      this.formload = false;
      this.childlist = result.result.data;
    },(error) => { this.formload = false; })
  }

  unsubscribe(subscription_id: any,stripe_id: any,username:any) {
   // this.upgrade(126,17);
   if(window.confirm('Are sure you want unsubscribe payment for this child ? child profile will be inactived after unsubscribe .')){
    //put your delete method logic here
   
      this.formload = true;
      this.process_username = username;
      let formdata = {subscription_id:subscription_id,stripe_id:stripe_id};
      this.api.submitAuth("parent/unsubscribe", formdata).subscribe(result => {
        this.formload = false;
        this.process_username = "";

        for(let i=0;i<this.childlist.length; i++){
          if(this.childlist[i].id == result.data.id){
            this.childlist[i].status = 0;
          }
        }
        let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':result.message,'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
      // this.notification = this.notification.filter(item => item.id !== id);
      },(error) => { this.process_username = ""; this.formload = false; this.displayError(error);}) 
    }
  }

  

  openDetail(myData: any) {
    this.userdetail = myData;
  }
  displayError(error:any) {
    this.formload = false;
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
 }

}
