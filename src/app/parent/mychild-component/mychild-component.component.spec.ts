import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MychildComponentComponent } from './mychild-component.component';

describe('MychildComponentComponent', () => {
  let component: MychildComponentComponent;
  let fixture: ComponentFixture<MychildComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MychildComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MychildComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
