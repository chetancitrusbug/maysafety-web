import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { CommonEventsService } from "../../common-events.service";
import { mychildlist } from "./../../app-data";
import { ccodes } from "./../../c-codes";
import { AuthApiService } from './../../default/auth-api.service';
import { EditChildValidation } from '../../service/validation/edit-child-validation'; 


@Component({
  selector: 'app-phone-verification',
  templateUrl: './phone-verification.component.html',
  styleUrls: ['./phone-verification.component.css']
})
export class PhoneVerificationComponent implements OnInit {
  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  @ViewChild('fileInput') fileInput:ElementRef;

  submitted = false;
  formload = false;
  
  public verificationData: any = {};
  public profile: any = {};
  public sformData: any = {};
  public pagedata: any = mychildlist;
  passForm : FormGroup;
  public my_phone_no: string = "";
  public call_status: number = 1;

  success:boolean = false;
  call:boolean = false;
  showTimer:boolean = true;

  timeLeft: number = 10;
  interval;
  
  phoOb :any = null;
  country_code : any = "+1";

  constructor(private fib: AngularFireDatabase,formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, public api: AuthApiService, private subscription: CommonEventsService) { 

    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    this.profile = authuser;
	this.country_code = authuser.country_code;

    let items = fib.object('phone-verification/'+this.profile.id).valueChanges().subscribe(items => {
      
      this.checkisVerified(items);
    });
    this.passForm = formBuilder.group({
      'phone' : [this.profile.phone, Validators.compose([Validators.required, Validators.maxLength(15)])]
    },{'validator': EditChildValidation.Formvalidation});
  }

  ngOnInit() {
    this.getPhonedata("");
  }
  gotoRoute(route:string) {
	 clearInterval(this.interval);
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  checkisVerified(verifingdata:any){
    
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    if(verifingdata && verifingdata.user_id && this.my_phone_no!= ""){
      if(verifingdata.user_id == authuser.id && verifingdata.phone_number == this.my_phone_no && verifingdata.verified == 1){
        this.call_status = 3;
        authuser.phone_verified = 1;
        authuser.country_code = this.sformData.country_code;
        authuser.phone =this.sformData.phone;
        localStorage.setItem('authuser',JSON.stringify(authuser));
        this.startTimer(true);
      }else{
        this.call_status = 2;
      }
    }
  }
  redirectifverified(){
    let authuser = JSON.parse(localStorage.getItem("authuser"));
    this.fileInput.nativeElement.click();
	clearInterval(this.interval);
    if(authuser && authuser.phone_verified == 1){
      let sucsess_ob =  {action:'flash_success',redirect_to:'parent/profile',dataobj:{'message':"Thank you! Your phone number has been verified.",'message_head':'Success !'}};
      this.subscription.globleEvent(sucsess_ob);
    }

    
    this.call_status = 0;
  }

  getPhonedata(myData: any) {
    this.formload = true;
    this.api.getAuthData("parent/get-phoneverification-code", myData).subscribe(result => {
      this.formload = false;
      this.verificationData = result.result;
	  this.country_code = this.verificationData.country_code;
	  this.dochangeCountry(this.country_code);
    },(error) => { this.formload = false; })
  }

  onSubmit(formData:any) {
    
    if(this.formload){
      return;
    }
    if(!this.verificationData){
      return;
    }
    if (this.passForm.invalid) {
      return;
    }else{
      this.formload = true;
        formData.phone = formData.phone.replace(/[^0-9]/g, '');
        this.sformData = formData;
        this.my_phone_no = this.country_code+formData.phone;
		formData.country_code=this.country_code; 
        formData.otp = this.verificationData.otp;
        this.call_status = 1;
        this.api.submitAuth("parent/phoneverification-call",formData).subscribe(result => {
          
      //    let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':result.message,'message_head':'Success !'}};
       //   this.subscription.globleEvent(sucsess_ob);
        
          this.formload = false;
        },
        (error) => { this.closemodal(); this.formload = false; this.displayError(error);})
    }
  }

  closemodal() {
    this.fileInput.nativeElement.click();
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
  startTimer(data){
    if(data = true){
      this.interval = setInterval(() => {
        if(this.timeLeft > 0) {
          this.timeLeft--;
          if(this.timeLeft < 10){}
        } else {
          this.timeLeft = 10;
          this.redirectifverified();
        }
      },1000)
    }
  }




displayError(error:any) {
  this.enableForm();
  let errMsg = (error.message) ? error.message :
  error.status ? `${error.status} - ${error.statusText}` : 'Server error';

  if(error.error && error.error.message){
    errMsg = error.error.message
  }
  let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
  this.subscription.globleEvent(error_ob);
}
enableForm(){
  let that = this;
  setTimeout(function(){
    that.formload = false;
  },2000);
}

  getNumber(obj) {
   // console.log(obj);
  }
  onCountryChange(obj) {
    this.country_code = "+"+obj.dialCode;
  }
  hasError(obj) {
    //console.log(obj);
  }
  telInputObject(obj) {
    this.phoOb = obj;
	this.dochangeCountry(this.country_code);
  }
  dochangeCountry(dcode:any){
	for (let i = 0; i < ccodes.length; i++) {
       if (ccodes[i].dial_code == dcode) {
			this.phoOb.intlTelInput('setCountry',ccodes[i].code);
			break;
       }
    }
	
  }
}
