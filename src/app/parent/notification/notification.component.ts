import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from './../../default/auth-api.service';  
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';  
import { parentnotification } from "./../../app-data";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  allids = "";
  submitted = false;
  profileForm : FormGroup;
  formload = false;
  public pagedata: any = parentnotification;
 
  public profile: any = {};
  public first_requst: any = {};
  public first_requst_user: any = {};
  public second_requst_user: any = {};
  public notification: any = [];

  constructor(private route: ActivatedRoute, private router: Router,public api: AuthApiService,private subscription: CommonEventsService) {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    this.profile = authuser;

  }

  ngOnInit() {
    this.getMyNotification("");
  }

  getMyNotification(myData: any) {
    this.api.getAuthData("child/get_notification", myData).subscribe(result => {
        this.notification = result.result;
        for(let i=0;i<this.notification.length; i++){
          if(this.allids == ""){
            this.allids = this.notification[i].id;
          }else{
            this.allids = this.allids+","+this.notification[i].id;
          }
        }
        this.getFirstRequest();
    },
    (error) => { })
  }

  getFirstRequest() {
    if(this.notification && this.notification.length >0 && this.profile){
        this.first_requst = this.notification[0];

        if(this.profile.id == this.first_requst.child_a_id){
          this.first_requst_user = this.first_requst.sender;
          this.second_requst_user = this.first_requst.receiver;
        }else{
          this.first_requst_user = this.first_requst.receiver;
          this.second_requst_user = this.first_requst.sender;
        }
    }
  }

  ngOnDestroy() {
    if(this.first_requst && this.first_requst.id){
      this.markdone("all",false);
    }
  }
  
  markdone(id:any,notify:boolean) {
    
    if(notify){
      this.formload = true;
    }
    let formData = {notification_id:id,read_flag:1};
    this.api.submitAuth("parent/update_notification",formData).subscribe(result => {
      
      if(notify){
        this.enableForm();
        this.notification = [];
        this.getFirstRequest();
        let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':'Success','message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
      }
      this.subscription.globleEvent({action:'load_notification',redirect_to:'',dataobj:{'message':'','message_head':''}});
     },    (error) => {
      
      if(notify){
        this.enableForm();
          let sucsess_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':'Something went wrong please try again ! ','message_head':'Success !'}};
          this.subscription.globleEvent(sucsess_ob);
        }
     })
     
     
  }

  enableForm(){
    let that = this;
    setTimeout(function(){
      that.formload = false;
    },2000);
  }
}
