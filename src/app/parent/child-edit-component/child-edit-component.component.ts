import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ccodes } from "./../../c-codes"; 
import { AuthApiService } from './../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { EditChildValidation } from '../../service/validation/edit-child-validation';
import * as moment from 'moment';
import { config } from "../../app-data";

@Component({
  selector: 'app-child-edit-component',
  templateUrl: './child-edit-component.component.html',
  styleUrls: ['./child-edit-component.component.css']
})
export class ChildEditComponentComponent implements OnInit {

  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  childForm: FormGroup;
  submitted = false;
  formload = false;
  previewImg : any= "";

  formBuilder: FormBuilder;


  item: any = {};
  user_id: number = 0;
  maxDate: Date = new Date();
  
  phoOb :any = null;
  country_code : any = "+1";

  constructor(formBuilder: FormBuilder, public api: AuthApiService, private subscription: CommonEventsService, private route: ActivatedRoute, private router: Router) {

    this.formBuilder = formBuilder;
    this.createForm(this.item);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.id && params.id != '') {
        this.user_id = params.id;
        this.getChildDetail(params.id);
      }
    });
  }

  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

  createForm(item: any) {
    this.childForm = this.formBuilder.group({
      'child_id': [item.id, Validators.required],
      'first_name': [item.first_name, Validators.required],
      'last_name': [item.last_name, Validators.required],
      'phone': [item.phone, Validators.compose([Validators.required,  Validators.maxLength(20)])],
      'school_name': [item.school_name, Validators.required],
      'school_district_no': [item.school_district_no, Validators.required],
      'state': [item.state, Validators.required],
      'image': null
    },{'validator': EditChildValidation.Formvalidation});
  }

  onFileChange(event:any) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.childForm.get('image').setValue(file);

      var reader = new FileReader();
      
      reader.onload = (event) => {
        this.previewImg = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
    } else {
      this.childForm.get('image').setValue(null);
      this.previewImg = "";
    }

    
  }

  private prepareSave(): any {
   
    let input = new FormData();
    input.append('child_id', this.childForm.get('child_id').value);
    input.append('first_name', this.childForm.get('first_name').value);
    input.append('last_name', this.childForm.get('last_name').value);
    input.append('school_name', this.childForm.get('school_name').value);
    input.append('school_district_no', this.childForm.get('school_district_no').value);
    input.append('state', this.childForm.get('state').value);
    input.append('country_code',this.country_code);

    if (this.childForm.get('image').value) {
      input.append('image', this.childForm.get('image').value);
    }
    if (this.childForm.get('phone').value) {
      let aphone = this.childForm.get('phone').value;
      input.append('phone', aphone.replace(/[^0-9]/g, ''));
    }
  

    return input;
  }

  getChildDetail(slug: any) {

    this.api.getAuthData("parent/childDetail?child_id=" + slug, []).subscribe(result => {
      this.item = result.result;
	  this.country_code = this.item.country_code;
	  this.createForm(this.item);
	  this.dochangeCountry(this.country_code);
     
    },
      (error) => {
         this.formload = false; 
         this.subscription.globleEvent({action:'redirect',redirect_to:"parent/child",dataobj:{}});
      })
  }



  onSubmit(formData: any) {
    this.submitted = true;
    if (this.childForm.invalid) {
      return;
    } else {
      this.formload = true;
      let formvalue = this.prepareSave();
      console.log(formvalue);
      this.upload("parent/updateChild", formvalue);
     
    }
  }

  

  public upload(modal: string, myData: any): Promise<any> {
    
    return new Promise((resolve, reject) => {
      let formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          this.enableForm();
          if (xhr.status === 200) {
            this.getChildDetail(this.user_id);
            this.displaySuccess(JSON.parse(xhr.response));
            resolve(JSON.parse(xhr.response));
          } else {
            this.displayError(JSON.parse(xhr.response));
            reject(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        //this.progress = Math.round(event.loaded / event.total * 100);
        //this.progressObserver.next(this.progress);
      };

      let token = "";
      let authuser: any = JSON.parse(localStorage.getItem("authuser"));
      if (authuser && authuser.token != '') {
        token = authuser.token;
      }
      xhr.open('POST', config.api_url + modal, true);
      xhr.setRequestHeader('Authorization', 'bearer ' + token);
      xhr.send(myData);
    });
  }

  displaySuccess(result: any) {
    this.previewImg = "";
    let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':result.message,'message_head':'Success !'}};
    this.subscription.globleEvent(sucsess_ob);
  }
  displayError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if (error && error.message) {
      errMsg = error.message;
    }
    let error_ob = { action: 'flash_error', redirect_to: '', dataobj: { 'message': errMsg, 'message_head': 'Request Not Procceed !' } };
    this.subscription.globleEvent(error_ob);
  }
  enableForm(){
    let that = this;
    setTimeout(function(){
      that.formload = false;
    },2000);
  }
  
  
  
getNumber(obj) {
   // console.log(obj);
  }
  onCountryChange(obj) {
    this.country_code = "+"+obj.dialCode;
  }
  hasError(obj) {
    //console.log(obj);
  }
  telInputObject(obj) {
    this.phoOb = obj;
	this.dochangeCountry(this.country_code);
  }
  dochangeCountry(dcode:any){
	for (let i = 0; i < ccodes.length; i++) {
       if (ccodes[i].dial_code == dcode) {
			this.phoOb.intlTelInput('setCountry',ccodes[i].code);
			break;
       }
    }
	
  }
}
