import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildEditComponentComponent } from './child-edit-component.component';

describe('ChildEditComponentComponent', () => {
  let component: ChildEditComponentComponent;
  let fixture: ComponentFixture<ChildEditComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildEditComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildEditComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
