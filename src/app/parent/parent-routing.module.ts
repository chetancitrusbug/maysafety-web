import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { AddChildComponent } from './add-child/add-child.component';
import { OrdersComponent } from './orders/orders.component';
import { NotificationComponent } from './notification/notification.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MychildComponentComponent } from './mychild-component/mychild-component.component';
import { ChildEditComponentComponent } from './child-edit-component/child-edit-component.component';
import { UpgradeComponentComponent } from './upgrade-component/upgrade-component.component';
import { DocumentComponentComponent } from './document-component/document-component.component';
import { CardsComponentComponent } from './cards-component/cards-component.component';
import { PhoneVerificationComponent } from './phone-verification/phone-verification.component';
import { PhoneverificationGardService } from '../service/phoneverification-gard.service';

const routes: Routes = [

  {path: '', canActivate:[PhoneverificationGardService], children: [
    {
      path: '',
      component: MyaccountComponent
    },
    {
      path: 'profile',
      component: MyaccountComponent
    },
    {
      path: 'add-child',
      component: AddChildComponent
    },
    {
      path: 'child',
      component: MychildComponentComponent
    },
    {
      path: 'child/:id',
      component: ChildEditComponentComponent
    },
    {
      path: 'child/subscription/:id',
      component: UpgradeComponentComponent
    },
    {
      path: 'child-documents/:id',
      component: DocumentComponentComponent
    },
    {
      path: 'orders',
      component: OrdersComponent
    },
    {
      path: 'notification',
      component: NotificationComponent
    },
    {
      path: 'change-password',
      component: ChangePasswordComponent
    },
    {
      path: 'manage-stripe-account',
      component: CardsComponentComponent
    }
    
  ]},
  {
    path: 'phone-verification',
    component: PhoneVerificationComponent
  }
  
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParentRoutingModule { }
