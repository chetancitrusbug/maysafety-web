import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from './../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';
import * as moment from 'moment';
import { config } from "../../app-data";
import { childsubscription } from "../../app-data";

@Component({
  selector: 'app-upgrade-component',
  templateUrl: './upgrade-component.component.html',
  styleUrls: ['./upgrade-component.component.css']
})
export class UpgradeComponentComponent implements OnInit {

  childForm: FormGroup;
  submitted = false;
  formload = false;
  public childsubscription: any = childsubscription;
  public pagedata: any = childsubscription.step_list;
  public planlist: any[] = [];

  formBuilder: FormBuilder;

  currentstep : string= "first";
  selected_plan: any = {};
  item: any = {};
  user_id: number = 0;
  current_subscription: any = null;
  maxDate: Date = new Date();

  constructor(formBuilder: FormBuilder, public api: AuthApiService, private subscription: CommonEventsService, private route: ActivatedRoute, private router: Router) {

    this.formBuilder = formBuilder;
    
  }

  ngOnInit() {
   
    this.route.params.subscribe(params => {
      if (params.id && params.id != '') {
        this.user_id = params.id;
        this.getPricing(params.id);
      //  this.getChildDetail(params.id);
      }
    });
  }

  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

  

  getPricing(myData:any) {
      
    this.formload = true;
    this.api.getAuthData("parent/get-plans-for-child?child_id="+myData,myData).subscribe(result => {
      this.formload = false;
      this.planlist = result.result.data;

      if(result.result.current_plan && result.result.current_plan){
        this.current_subscription = result.result.current_plan;
      }
      if(result.result.child){
        this.item = result.result.child;
      }
    },
    (error) => { this.formload = false; })
}

  
/*
  getChildDetail(slug: any) {

    this.api.getAuthData("parent/childDetail?child_id=" + slug, []).subscribe(result => {
     // this.item = result.result;
      if(this.item.current_subscription && this.item.current_subscription){
        console.log(this.current_subscription);
      }
    },
      (error) => { this.formload = false; })
  }*/

  gotoStep(step:string) {
    if(step == "first"){  this.pagedata = childsubscription.step_list; }
    else if(step == "upgrade"){  this.pagedata = childsubscription.step_upgrade; }
    else if(step == "unsubscribe"){  this.pagedata = childsubscription.step_unsubscribe; }

    this.currentstep = step;
    this.subscription.globleEvent({ action: 'scroll_top', redirect_to: '', dataobj: { 'message': '', 'message_head': '' } });
  }

  upgrade(plan: any,step:any) {

    if(step == "first"){  this.pagedata = childsubscription.step_list; }
    else if(step == "upgrade"){  this.pagedata = childsubscription.step_upgrade; }
    else if(step == "unsubscribe"){  this.pagedata = childsubscription.step_unsubscribe; }
    

    this.selected_plan = plan;
    
    if(this.selected_plan && this.item){
      this.currentstep = step;
    }

    this.subscription.globleEvent({ action: 'scroll_top', redirect_to: '', dataobj: { 'message': '', 'message_head': '' } });
  }

  unsubscribe(subscription_id: any,stripe_id: any,username:any) {
   
    if(true){
      this.formload = true;
       
       let formdata = {subscription_id:subscription_id,stripe_id:stripe_id};
       this.api.submitAuth("parent/unsubscribe", formdata).subscribe(result => {
         this.formload = false;
         let sucsess_ob =  {action:'flash_success',redirect_to:'parent/child',dataobj:{'message':result.message,'message_head':'Success !'}};
         this.subscription.globleEvent(sucsess_ob);
      },(error) => { this.formload = false; this.displayError(error);}) 
     }
   }

  doupgrade() {
    let child_id = this.user_id;
    let plan_id = this.selected_plan.id;
    
    this.formload = true;
    let formdata = {child_id:child_id,plan_id:plan_id};
    this.api.submitAuth("parent/upgrade-subscription", formdata).subscribe(result => {
     // this.notification = this.notification.filter(item => item.id !== id);
      let sucsess_ob =  {action:'flash_success',redirect_to:'parent/child',dataobj:{'message':result.message,'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
    },(error) => { this.formload = false; this.displayError(error); })
  }



  displaySuccess(result: any) {
    let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':result.message,'message_head':'Success !'}};
    this.subscription.globleEvent(sucsess_ob);
  }
  
  displayError(error: any) {
    //this.enableForm();
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if (error.error && error.error.message) {
      errMsg = error.error.message
    }
    let error_ob = { action: 'flash_error', redirect_to: '', dataobj: { 'message': errMsg, 'message_head': 'Request Not Procceed !' } };
    this.subscription.globleEvent(error_ob);
  }
  enableForm(){
    let that = this;
    setTimeout(function(){
      that.formload = false;
    },2000);
  }
}
