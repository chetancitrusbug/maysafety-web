import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from './../../default/auth-api.service';  
import { CommonEventsService } from "../../common-events.service";
import { ccodes } from "./../../c-codes";
import { EditChildValidation } from '../../service/validation/edit-child-validation'; 
import { myaccount } from "./../../app-data";

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {

  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public pagedata: any = myaccount;
  submitted = false;
  profileForm : FormGroup;
  formload = false;
 
  public profile: any = {};
  
  phoOb :any = null;
  country_code : any = "+1";

  constructor(formBuilder: FormBuilder,private route: ActivatedRoute, private router: Router,public api: AuthApiService,private subscription: CommonEventsService) {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
   // console.log(authuser);
    this.profile = authuser;
	this.country_code = authuser.country_code;

    this.profileForm = formBuilder.group({
      'first_name' : [this.profile.first_name, Validators.required],
      'last_name': [this.profile.last_name, Validators.required],
      'phone' : [this.profile.phone, Validators.compose([Validators.required, Validators.maxLength(15)])]
    },{'validator': EditChildValidation.Formvalidation});
  }

  ngOnInit() {
    

    this.getProfile("");
  }

  getProfile(myData:any) {
    
    
    this.api.getAuthData("parent/detail",myData).subscribe(result => {
      this.formload = false;
      this.profile = result.result;
	  this.country_code = this.profile.country_code;
	  this.dochangeCountry(this.country_code);
    },
    (error) => { this.formload = false; })
}

onSubmit(formData:any) {
  let authuser: any = JSON.parse(localStorage.getItem("authuser"));
  this.submitted = true;
 
  if (this.profileForm.invalid) {
    return;
 }else{
  this.formload = true;
  formData.phone = formData.phone.replace(/[^0-9]/g, '');
  formData.country_code=this.country_code; 
  
    this.api.submitAuth("parent/register/edit",formData).subscribe(result => {
      authuser.first_name = result.result.first_name;
      authuser.last_name = result.result.last_name;
      authuser.phone = result.result.phone;
      authuser.country_code = result.result.country_code;
      authuser.phone_verified = result.result.phone_verified;
      let redirect_to = "";
      if(result.result.phone_verified == 0){
        redirect_to = "parent/phone-verification";
      }
      this.enableForm();
      localStorage.setItem('authuser',JSON.stringify(authuser));
      this.subscription.globleEvent({action:'reset_header',redirect_to:'',dataobj:{'message':'','message_head':''}});
      this.subscription.globleEvent({action:'flash_success',redirect_to:redirect_to,dataobj:{'message':result.message,'message_head':'Success !'}});
    },
    (error) => {this.displayError(error);})
 }
}

displayError(error:any) {
  this.enableForm();
  let errMsg = (error.message) ? error.message :
  error.status ? `${error.status} - ${error.statusText}` : 'Server error';

  if(error.error && error.error.message){
    errMsg = error.error.message
  }
  let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
  this.subscription.globleEvent(error_ob);
}
enableForm(){
  let that = this;
  setTimeout(function(){
    that.formload = false;
  },2000);
}


getNumber(obj) {
   // console.log(obj);
  }
  onCountryChange(obj) {
    this.country_code = "+"+obj.dialCode;
  }
  hasError(obj) {
    //console.log(obj);
  }
  telInputObject(obj) {
    this.phoOb = obj;
	this.dochangeCountry(this.country_code);
  }
  dochangeCountry(dcode:any){
	for (let i = 0; i < ccodes.length; i++) {
       if (ccodes[i].dial_code == dcode) {
			this.phoOb.intlTelInput('setCountry',ccodes[i].code);
			break;
       }
    }
	
  }
}
