import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from '../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';  


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  passForm : FormGroup;
  submitted = false;
  formload = false;

  maxDate: Date = new Date();
  constructor(formBuilder: FormBuilder, public api: AuthApiService,private subscription: CommonEventsService) { 
    
    this.passForm = formBuilder.group({
      'password' : [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      'confirm_password' : []
    },{'validator': CustomValidation.MatchPassword});
  }

  ngOnInit() {
  }


  onSubmit(formData:any) {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    this.submitted = true;
   
    if (this.passForm.invalid) {
      return;
   }else{
      this.formload = true;
      formData.email = authuser.email;
      this.api.submitAuth("parent/change_password",formData).subscribe(result => {
        this.formload = false;
        let sucsess_ob =  {action:'flash_success',redirect_to:'parent',dataobj:{'message':result.message,'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
        
      },
      (error) => {this.displayError(error);})
   }
 }

 displayError(error:any) {
  this.formload = false; 
  let errMsg = (error.message) ? error.message :
  error.status ? `${error.status} - ${error.statusText}` : 'Server error';

  if(error.error && error.error.message){
    errMsg = error.error.message
  }
  let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
  this.subscription.globleEvent(error_ob);
}

}
