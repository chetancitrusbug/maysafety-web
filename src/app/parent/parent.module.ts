import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { NgxStripeModule } from 'ngx-stripe';
import { TextMaskModule } from 'angular2-text-mask';
import {Ng2TelInputModule} from 'ng2-tel-input';

import { ParentRoutingModule } from './parent-routing.module';
import { AddChildComponent } from './add-child/add-child.component';
import { OrdersComponent } from './orders/orders.component';
import { NotificationComponent } from './notification/notification.component';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MychildComponentComponent } from './mychild-component/mychild-component.component';
import { AuthApiService } from '../default/auth-api.service';
import { ChildEditComponentComponent } from './child-edit-component/child-edit-component.component';
import { UpgradeComponentComponent } from './upgrade-component/upgrade-component.component';

import { config } from "../app-data";
import { DocumentComponentComponent } from './document-component/document-component.component';
import { ListLoaderComponent } from './list-loader/list-loader.component';
import { CardsComponentComponent } from './cards-component/cards-component.component';
import { CardLoaderComponentComponent } from './card-loader-component/card-loader-component.component';
import { PhoneVerificationComponent } from './phone-verification/phone-verification.component';
import { PhoneverificationGardService } from '../service/phoneverification-gard.service';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ParentRoutingModule,
    TextMaskModule,
    BsDatepickerModule.forRoot(),
    NgxStripeModule.forRoot(config.PUBLISHABLE_KEY),
	Ng2TelInputModule
  ],
  declarations: [
    AddChildComponent,
    OrdersComponent,
    NotificationComponent,
    MyaccountComponent,
    ChangePasswordComponent,
    MychildComponentComponent,
    ChildEditComponentComponent,
    UpgradeComponentComponent,
    DocumentComponentComponent,
    ListLoaderComponent,
    CardsComponentComponent,
    CardLoaderComponentComponent,
    PhoneVerificationComponent
  ],
  providers: [AuthApiService,PhoneverificationGardService]
})
export class ParentModule { }
