import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
 
import { ccodes } from "./../../c-codes"; 
import { addchild } from "./../../app-data";
import { AuthApiService } from './../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { AddChildValidation } from '../../service/validation/add-child-validation';  
import * as moment from 'moment'; 

@Component({
  selector: 'app-add-child',
  templateUrl: './add-child.component.html',
  styleUrls: ['./add-child.component.css']
})
export class AddChildComponent implements OnInit {

  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public pagedata: any = addchild;
  elements: Elements;
  card: StripeElement;

  isunique_phone : boolean = true;
  isunique_username : boolean = true;
 
  // optional parameters
  elementsOptions: ElementsOptions = {
    locale: 'en'
  };

  childForm : FormGroup;
  submitted = false;

  currentstep : string= "first";
  formload = false;
  
  phoOb :any = null;
  country_code : any = "+1";

  maxDate: Date = new Date(new Date().setDate(new Date().getDate() - 365));
  minDate: Date = new Date(new Date().setDate(new Date().getDate() - 365*18));
  public planlist: any[] = [];
	
  constructor(formBuilder: FormBuilder, public api: AuthApiService,private subscription: CommonEventsService,private stripeService: StripeService) { 
    
    this.childForm = formBuilder.group({
      'plan_id': null,
      'token_id': null,
      'first_name' : [null, Validators.required],
      'last_name': [null, Validators.required],
      'age': [],
      'dob': [null, Validators.required],
      'gender' : ['male', Validators.required],
      'phone' : [null, Validators.compose([Validators.required,Validators.maxLength(15)])],
      'school_name' : [null, Validators.required],
      'school_district_no' : [null, Validators.required],
      'state' : [null, Validators.required],
      'username' : [null, Validators.required],
      'password' : [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      'confirm_password' : []
    },{'validator': AddChildValidation.MatchPassword});
  }

  ngOnInit() {
    this.getPricingList();
    this.stripeService.elements(this.elementsOptions)
    .subscribe(elements => {
      this.elements = elements;
      // Only mount the element the first time
      if (!this.card) {
        this.card = this.elements.create('card', {
          style: {
            base: {
              iconColor: '#666EE8',
              color: '#31325F',
              lineHeight: '40px',
              fontWeight: 300,
              fontSmoothing: 'antialiased',
              fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
              fontSize: '18px',
              '::placeholder': {
                color: 'purple'
              }
            }
          }
        });
        this.card.mount('#card-element');
      }
    });
  }

  getPricingList() {
    this.api.getAuthData("parent/subscription-list","").subscribe(result => {
      this.planlist = result.result.data;
    },
    (error) => {  })
  }
  verifyForm(field:string) {
    let modal = "";
    let phones = "";
    let formdata = {};
    if(field == 'phone'){
      modal = "parent/unique_child_number";
      phones = this.childForm.value.phone.replace(/[^0-9]/g, '');
      formdata = {phone:phones};
    }else if(field == 'username'){
      modal = "parent/unique_child_user_name";
      formdata = {user_name:this.childForm.value.username};
    }
    
    this.api.submitAuth(modal,formdata).subscribe(result => {
      if(field == 'phone'){
        this.isunique_phone = true;
      }else if(field == 'username'){
        this.isunique_username = true;
      }
    },
    (error) => { 
      if(field == 'phone'){
        this.isunique_phone = false;
      }else if(field == 'username'){
        this.isunique_username = false;
      }

     })
  }
  processStep1(formData:any) {
    this.submitted = true;
   if (this.childForm.invalid) {
      return;
   }else{
    
      this.childForm.get('dob').setValue(moment(formData.dob).format('YYYY-MM-DD'));
      this.gotoStep('second');
      
   }
 }
 processStep2(plan_id:any) {
    this.childForm.get('plan_id').setValue(plan_id);
    this.gotoStep('third');
 }

 gotoStep(step:string) {
  this.currentstep = step;
  this.subscription.globleEvent({ action: 'scroll_top', redirect_to: '', dataobj: { 'message': '', 'message_head': '' } });
}

 processStep3() {
  this.formload =true
  const name = this.childForm.value.first_name;
  this.stripeService
    .createToken(this.card, { name })
    .subscribe(result => {
      if (result.token) {
       
        this.childForm.get('token_id').setValue(result.token.id);
        this.onSubmit(this.childForm.value);
      } else if (result.error) {
        this.formload = false;
        let error_ob = { action: 'flash_error', redirect_to: '', dataobj: { 'message': result.error.message, 'message_head': 'Request Not Procceed !' } };
        this.subscription.globleEvent(error_ob);
      
      }
    });
}

onSubmit(formData:any) {
 
    this.submitted = true;
   
    if (this.childForm.invalid) {
      return;
   }else{
      formData.phone = this.childForm.value.phone.replace(/[^0-9]/g, '');
	  formData.country_code=this.country_code; 
	  
      this.api.submitAuth("parent/child-register",formData).subscribe(result => {
        this.formload = false;
        let sucsess_ob =  {action:'flash_success',redirect_to:'parent/child',dataobj:{'message':result.message,'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
        
      },
      (error) => { this.formload = false; this.displayError(error);})
   }
   
 }

 displayError(error:any) {
  let errMsg = (error.message) ? error.message :
  error.status ? `${error.status} - ${error.statusText}` : 'Server error';

  if(error.error && error.error.message){
    errMsg = error.error.message
  }
  let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
  this.subscription.globleEvent(error_ob);
}



getNumber(obj) {
   // console.log(obj);
  }
  onCountryChange(obj) {
    this.country_code = "+"+obj.dialCode;
  }
  hasError(obj) {
    //console.log(obj);
  }
  telInputObject(obj) {
    this.phoOb = obj;
	this.dochangeCountry(this.country_code);
  }
  dochangeCountry(dcode:any){
	for (let i = 0; i < ccodes.length; i++) {
       if (ccodes[i].dial_code == dcode) {
			this.phoOb.intlTelInput('setCountry',ccodes[i].code);
			break;
       }
    }
	
  }
  
}
