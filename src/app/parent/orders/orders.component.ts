import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { myorders } from "./../../app-data";
import { AuthApiService } from './../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  submitted = false;
  formload = false;
  public orderlist: any[] = [];
  public orderdetail: any = {};
  public orderdetailplan: any = {};
  public pagedata: any = myorders;

  constructor(formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, public api: AuthApiService, private subscription: CommonEventsService) {
    
  }

  ngOnInit() {
    this.getOrderList("");
  }

  getOrderList(myData: any) {
    this.formload = true;
    this.api.getAuthData("parent/order_detail", myData).subscribe(result => {
      this.formload = false;
      this.orderlist = result.result;
    },(error) => { this.formload = false; })
  }
  openDetail(myData: any) {
    this.orderdetail = myData;
    this.orderdetailplan = JSON.parse(this.orderdetail.auth_response);
console.log(this.orderdetailplan);
  }

}
