import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthApiService } from './../../default/auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { EditChildValidation } from '../../service/validation/edit-child-validation';
import * as moment from 'moment';
import { config } from "../../app-data";

@Component({
  selector: 'app-document-component',
  templateUrl: './document-component.component.html',
  styleUrls: ['./document-component.component.css']
})
export class DocumentComponentComponent implements OnInit {

  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  childForm: FormGroup;
  submitted = false;
  formload = false;
  previewImg : any= "";
  parental_permission_form_url : any= config.parental_permission_form;

  formBuilder: FormBuilder;


  item: any = {};
  user_id: number = 0;
  maxDate: Date = new Date();

  constructor(formBuilder: FormBuilder, public api: AuthApiService, private subscription: CommonEventsService, private route: ActivatedRoute, private router: Router) {

    this.formBuilder = formBuilder;
    this.createForm(this.item);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.id && params.id != '') {
        this.user_id = params.id;
        this.getChildDetail(params.id);
      }
    });
  }

  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

  createForm(item: any) {
    this.childForm = this.formBuilder.group({
      'child_id': [item.id, Validators.required],
      'document': [null, Validators.required]
    });
  }

  onFileChange(event:any,field:any) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.childForm.get(field).setValue(file);
      console.log(this.childForm);
/*
      var reader = new FileReader();
      
      reader.onload = (event) => {
        this.previewImg = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]); */
    } else {
      this.childForm.get(field).setValue(null);
     // this.previewImg = "";
    }

    
  }

  private prepareSave(): any {
   
    let input = new FormData();
    input.append('child_id', this.childForm.get('child_id').value);
      input.append('file', this.childForm.get('document').value);
      input.append('upload_type', "document");
    return input;
  }

  getChildDetail(slug: any) {

    this.api.getAuthData("parent/childDetail?child_id=" + slug, []).subscribe(result => {
      this.item = result.result;
      this.createForm(this.item);
    },
      (error) => { this.formload = false; })
  }



  onSubmit(formData: any) {
    this.submitted = true;
    if (this.childForm.invalid) {
      return;
    } else {
      this.formload = true;
      let formvalue = this.prepareSave();
      this.upload("parent/upload-document", formvalue);
     
    }
  }

  

  public upload(modal: string, myData: any): Promise<any> {
    
    return new Promise((resolve, reject) => {
      let formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          this.enableForm();
          if (xhr.status === 200) {
            this.getChildDetail(this.user_id);
            this.displaySuccess(JSON.parse(xhr.response));
            resolve(JSON.parse(xhr.response));
          } else {
            this.displayError(JSON.parse(xhr.response));
            reject(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        //this.progress = Math.round(event.loaded / event.total * 100);
        //this.progressObserver.next(this.progress);
      };

      let token = "";
      let authuser: any = JSON.parse(localStorage.getItem("authuser"));
      if (authuser && authuser.token != '') {
        token = authuser.token;
      }
      xhr.open('POST', config.api_url + modal, true);
      xhr.setRequestHeader('Authorization', 'bearer ' + token);
      xhr.send(myData);
    });
  }

  displaySuccess(result: any) {
   // this.createForm(this.item);
    this.previewImg = "";
    let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':result.message,'message_head':'Success !'}};
    this.subscription.globleEvent(sucsess_ob);
  }
  displayError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if (error && error.message) {
      errMsg = error.message;
    }
    let error_ob = { action: 'flash_error', redirect_to: '', dataobj: { 'message': errMsg, 'message_head': 'Request Not Procceed !' } };
    this.subscription.globleEvent(error_ob);
  }
  enableForm(){
    let that = this;
    setTimeout(function(){
      that.formload = false;
    },2000);
  }
}
