import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthApiService } from './../auth-api.service';  
import { CommonEventsService } from "../../common-events.service";

@Component({
  selector: 'app-child-sign-in',
  templateUrl: './child-sign-in.component.html',
  styleUrls: ['./child-sign-in.component.css']
})
export class ChildSignInComponent implements OnInit {

  signinForm : FormGroup;
  submitted = false;
  formload = false;

  constructor(formBuilder: FormBuilder,public api: AuthApiService,private subscription: CommonEventsService) {

    this.signinForm = formBuilder.group({
      'password' : [null, Validators.required],
      'username' :[null, Validators.required],
    });

   }

  ngOnInit() {
    
  }

  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  onSubmit(myData:any) {
    this.submitted = true;

    
   if (this.signinForm.invalid) {
        return;
   }else{
      this.formload = true;
      myData.user_type='CU';
      myData.device_type='web';
    //  myData.fire_base_token= (new Date().getTime()).toString(36);
      this.api.submit("child/login",myData).subscribe(result => {

        this.enableForm();
        let sucsess_ob =  {action:'flash_success',redirect_to:'child',dataobj:{'message':result.message,'message_head':'Success !'}};
        let authdata = result.result;

        authdata.user_type='CU';
        localStorage.setItem('authuser',JSON.stringify(authdata));
        this.subscription.globleEvent({action:'load_notification',redirect_to:'',dataobj:{'message':'','message_head':''}});
        this.subscription.globleEvent(sucsess_ob);
        this.subscription.globleEvent({action:'load_jquery',redirect_to:'',dataobj:{'message':'','message_head':''}});
      },
      (error) => {this.displayError(error);})
   }
 }
 displayError(error:any) {
    this.enableForm();
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
 }
 enableForm(){
  let that = this;
  setTimeout(function(){
    that.formload = false;
  },2000);
}

}
