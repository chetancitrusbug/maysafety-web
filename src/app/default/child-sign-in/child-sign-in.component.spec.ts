import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildSignInComponent } from './child-sign-in.component';

describe('ChildSignInComponent', () => {
  let component: ChildSignInComponent;
  let fixture: ComponentFixture<ChildSignInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildSignInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildSignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
