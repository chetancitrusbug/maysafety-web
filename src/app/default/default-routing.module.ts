import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { HomeComponent } from './home/home.component';


import { ParentSignUpComponent } from './parent-sign-up/parent-sign-up.component';
import { ParentSignInComponent } from './parent-sign-in/parent-sign-in.component';
import { ChildSignInComponent } from './child-sign-in/child-sign-in.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { SupportComponent } from './support/support.component';
import { PricingpageComponent } from './pricingpage/pricingpage.component';
import { FeaturespageComponent } from './featurespage/featurespage.component';

import { ChildForgotpassComponentComponent } from './child-forgotpass-component/child-forgotpass-component.component';
import { NonAuthGardService } from '../service/non-auth-gard.service';
import { ComplaintComponent } from './complaint/complaint.component';

const routes: Routes = [
  {
      path: '',
      component: HomeComponent,
      canActivate: [NonAuthGardService]
  },
  {
    path: 'parent-signup',
    component: ParentSignUpComponent,
    canActivate: [NonAuthGardService]
  },
  {
    path: 'parent-signin',
    component: ParentSignInComponent,
    canActivate: [NonAuthGardService]
  },
  {
    path: 'child-signin',
    component: ChildSignInComponent,
    canActivate: [NonAuthGardService]
  },
  {
    path: 'child-forgot-password',
    component: ChildForgotpassComponentComponent,
    canActivate: [NonAuthGardService]
  },
  {
    path: 'forgot-password',
    component: ForgotpasswordComponent,
    canActivate: [NonAuthGardService]
  },
  {
    path: 'blogs',
    component: BlogsComponent
  },
  {
    path: 'blog/:id',
    component: BlogDetailComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'anonymous-complaint',
    component: ComplaintComponent
  },
  {
    path: 'privacy',
    component: PrivacyComponent
  },
  {
    path: 'terms-conditions',
    component: TermsConditionsComponent
  },
  {
    path: 'support',
    component: SupportComponent
  },
  {
    path: 'features',
    component: FeaturespageComponent
  },
  {
    path: 'pricing',
    component: PricingpageComponent
  }      
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DefaultRoutingModule { }
