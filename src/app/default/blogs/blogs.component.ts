import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  

import { latest_news } from "./../../app-data";
import { AuthApiService } from './../auth-api.service';  
import { CommonEventsService } from "../../common-events.service";

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {

  public pagedata: any = latest_news;
  

  submitted = false;
  formload = false;
  next_page_url :number= 0;
  prev_page_url :number= 0;
  current_page :number= 0;
 
  public bloglist: any[] = [];
  

  constructor(private route: ActivatedRoute, private router: Router,public api: AuthApiService,private subscription: CommonEventsService) { }

  ngOnInit() {
    this.formload = true;
    this.getList("all");
  }
  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  getList(myData:any) {

    if(myData == 0){
      return ;
    }else if(myData !="all"){
      myData = "&page="+myData;
    }
    this.next_page_url =  0;
    this.prev_page_url = 0;
      
      this.api.getData("all-blogs?order_by=DESC"+myData,"").subscribe(result => {
        this.formload = false;
        this.bloglist = result.result;
        if(result.current_page){
          this.current_page = result.current_page;
          if(result.next_page_url && result.next_page_url!=""){
            this.next_page_url = this.current_page+1;
          }
          if(result.prev_page_url && result.prev_page_url!=""){
            this.prev_page_url = this.current_page-1;
          }
        }

        let ob =  {action:'scroll_top',redirect_to:'',dataobj:{'message':'','message_head':''}};
        this.subscription.globleEvent(ob);
      },
      (error) => { this.formload = false; })
  }
  displayError(error:any) {
    this.formload = false;
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
  }

  
}
