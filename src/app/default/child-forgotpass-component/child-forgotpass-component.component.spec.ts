import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildForgotpassComponentComponent } from './child-forgotpass-component.component';

describe('ChildForgotpassComponentComponent', () => {
  let component: ChildForgotpassComponentComponent;
  let fixture: ComponentFixture<ChildForgotpassComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildForgotpassComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildForgotpassComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
