import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthApiService } from './../auth-api.service';  
import { CommonEventsService } from "../../common-events.service";
import { CustomValidation } from '../../service/validation/custom-validation'; 

@Component({
  selector: 'app-child-forgotpass-component',
  templateUrl: './child-forgotpass-component.component.html',
  styleUrls: ['./child-forgotpass-component.component.css']
})
export class ChildForgotpassComponentComponent implements OnInit {

  forgotForm : FormGroup;
  passForm : FormGroup;
  submitted = false;
  submitteda = false;
  formload = false;
  step = "first";
  
  constructor(formBuilder: FormBuilder,public api: AuthApiService,private subscription: CommonEventsService) {

    this.forgotForm = formBuilder.group({
      'phone' : [null, Validators.compose([Validators.required])],
    });

    this.passForm = formBuilder.group({
      'username' : [null, Validators.compose([Validators.required])],
      'otp' : [null, Validators.compose([Validators.required])],
      'password' : [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      'confirm_password' : []
    },{'validator': CustomValidation.MatchPassword});

   }

  ngOnInit() {
  }

  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

  onSubmit(myData:any) {
    this.submitted = true;

    
   if (this.forgotForm.invalid) {
        return;
   }else{
        this.formload = true;
        this.api.submit("child/forgot_password",myData).subscribe(result => {
        this.formload = false;
        this.step="two";
        let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':"One time password has been sent to your registered mobile number",'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
      },
      (error) => {this.displayError(error);})
   }
 }
 resend(){
  this.step="first";
 }
 onResetPassword(formData:any) {
  
  this.submitteda = true;
 
  if (this.passForm.invalid) {
    return;
 }else{
    this.formload = true;
    this.api.submitAuth("child/update_forgot_password",formData).subscribe(result => {
      this.enableForm();
      let sucsess_ob =  {action:'flash_success',redirect_to:'child-signin',dataobj:{'message':result.message,'message_head':'Success !'}};
      this.subscription.globleEvent(sucsess_ob);
    },
    (error) => {this.displayError(error);})
 }
}

 displayError(error:any) {
    this.enableForm();
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
 }
 enableForm(){
  let that = this;
  setTimeout(function(){
    that.formload = false;
  },2000);
}

}