import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  

import { AuthApiService } from './../auth-api.service';  
import { CommonEventsService } from "../../common-events.service";

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {

  
  submitted_a = false;
  formload_a = false;
  submitted_b = false;
  formload_b = false;
 
  public bloglist: any[] = [];
  public blog: any = {};
  

  constructor(private route: ActivatedRoute, private router: Router,public api: AuthApiService,private subscription: CommonEventsService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if(params.id && params.id != ''){
        this.getBlogDetail(params.id);
      } 
    });
    this.getList("");
  }
  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  getList(myData:any) {
        
    this.formload_a = true;
    this.api.getData("all-blogs?order_by=DESC",myData).subscribe(result => {
      this.formload_a = false;
      this.bloglist = result.result;
    },
    (error) => { this.formload_a = false; })
  }
  getBlogDetail(slug:any) {
        
    this.formload_b = true;
    this.api.getData("get-blog?slug="+slug,[]).subscribe(result => {
      this.formload_b = false;
      this.blog = result.result;
    },
    (error) => { this.formload_b = false; })
  }
  displayError(error:any) {
    
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
  }

}
