import { Component, OnInit } from '@angular/core';
import { CommonEventsService } from "../../../common-events.service";
import { config } from "./../../../app-data";

@Component({
  selector: 'app-footer-start-free-trial',
  templateUrl: './footer-start-free-trial.component.html',
  styleUrls: ['./footer-start-free-trial.component.css']
})
export class FooterStartFreeTrialComponent implements OnInit {

  public config: any = config;
  constructor(private subscription: CommonEventsService) { }

  ngOnInit() {
  }

  gotoRoute(route:string){
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

}
