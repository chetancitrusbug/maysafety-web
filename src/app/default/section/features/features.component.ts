import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { features } from "./../../../app-data";
import { CommonEventsService } from "../../../common-events.service";

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.css']
})
export class FeaturesComponent implements OnInit {

  public pagedata: any = features;
  public isPage : boolean = false;
  constructor(private route: ActivatedRoute, private router: Router,private subscription: CommonEventsService) {
    
   }

  ngOnInit() {
    if(this.router.url == "/features" ){
      this.isPage = true;
    }
  }

  gotoRoute(route:string){
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

}
