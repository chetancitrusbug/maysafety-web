import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { pricingplans } from "./../../../app-data";
import { addchild } from "./../../../app-data";

import { AuthApiService } from './../../auth-api.service';  
import { CommonEventsService } from "../../../common-events.service";

@Component({
  selector: 'app-pricing-plans',
  templateUrl: './pricing-plans.component.html',
  styleUrls: ['./pricing-plans.component.css']
})
export class PricingPlansComponent implements OnInit {

  public pricingplans: any = pricingplans;
  public addchild: any = addchild;

  public isPage : boolean = false;

  submitted = false;
  formload = false;
 
  public planlist: any[] = [];

  constructor(private route: ActivatedRoute, private router: Router,public api: AuthApiService,private subscription: CommonEventsService) { }

  ngOnInit() {
    if(this.router.url == "/pricing" ){
      this.isPage = true;
    }
    this.getList("");
  }
  gotoRoute(route:string){
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  
  getList(myData:any) {
      
      this.formload = true;
      this.api.getData("parent/subscription-list",myData).subscribe(result => {
        this.formload = false;
        this.planlist = result.result.data;

      },
      (error) => { this.formload = false; })
 }
 displayError(error:any) {
    this.formload = false;
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
 }

}
