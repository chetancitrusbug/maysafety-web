import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderStartFreeTrialComponent } from './header-start-free-trial.component';

describe('HeaderStartFreeTrialComponent', () => {
  let component: HeaderStartFreeTrialComponent;
  let fixture: ComponentFixture<HeaderStartFreeTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderStartFreeTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderStartFreeTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
