import { Component, OnInit } from '@angular/core';
import { homepage } from "./../../../app-data";
import { config } from "./../../../app-data";
import { CommonEventsService } from "../../../common-events.service";

@Component({
  selector: 'app-header-start-free-trial',
  templateUrl: './header-start-free-trial.component.html',
  styleUrls: ['./header-start-free-trial.component.css']
})
export class HeaderStartFreeTrialComponent implements OnInit {

  public config: any = config;
  public homepage: any = homepage;
  constructor(private subscription: CommonEventsService) { }

  ngOnInit() {
  }

  gotoRoute(route:string){
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
}
