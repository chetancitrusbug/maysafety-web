import { Component, OnInit } from '@angular/core';
import { completeProtection } from "./../../../app-data";
import { config } from "./../../../app-data";
import { CommonEventsService } from "../../../common-events.service";

@Component({
  selector: 'app-complete-protection',
  templateUrl: './complete-protection.component.html',
  styleUrls: ['./complete-protection.component.css']
})
export class CompleteProtectionComponent implements OnInit {

  public completeProtection: any = completeProtection;
  public config: any = config;
  
  constructor(private subscription: CommonEventsService) { }

  ngOnInit() {
    
  }

  gotoRoute(route:string){
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }

}
