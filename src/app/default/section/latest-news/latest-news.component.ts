import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  

import { latest_news } from "./../../../app-data";
import { AuthApiService } from './../../auth-api.service';  
import { CommonEventsService } from "../../../common-events.service";

@Component({
  selector: 'app-latest-news',
  templateUrl: './latest-news.component.html',
  styleUrls: ['./latest-news.component.css']
})
export class LatestNewsComponent implements OnInit {

  public pagedata: any = latest_news;
  public isPage : boolean = false;

  submitted = false;
  formload = false;
 
  public bloglist: any[] = [];
  public blog: any = {};

  constructor(private route: ActivatedRoute, private router: Router,public api: AuthApiService,private subscription: CommonEventsService) { }

  ngOnInit() {
    this.getList("");
  }

  gotoRoute(route:string){
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  getList(myData:any) {
      
    this.formload = true;
    this.api.getData("all-blogs?order_by=DESC",myData).subscribe(result => {
      this.formload = false;
      this.bloglist = result.result;
      if(this.bloglist && this.bloglist.length > 0){
        this.blog =this.bloglist[0];
      }
    },
    (error) => { this.formload = false; })
}
displayError(error:any) {
  this.formload = false;
  let errMsg = (error.message) ? error.message :
  error.status ? `${error.status} - ${error.statusText}` : 'Server error';

  if(error.error && error.error.message){
    errMsg = error.error.message
  }
  let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
  this.subscription.globleEvent(error_ob);
}
}
