import { Component, OnInit } from '@angular/core';
import { howitswork } from "./../../../app-data";

@Component({
  selector: 'app-how-it-work',
  templateUrl: './how-it-work.component.html',
  styleUrls: ['./how-it-work.component.css']
})
export class HowItWorkComponent implements OnInit {

  public howitswork: any = howitswork;
  constructor() { }

  ngOnInit() {
  }

}
