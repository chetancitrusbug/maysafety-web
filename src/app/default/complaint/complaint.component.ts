import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  

import { AuthApiService } from './../auth-api.service';  
import { CommonEventsService } from "../../common-events.service";
import { complaint } from "./../../app-data";

@Component({
  selector: 'app-complaint',
  templateUrl: './complaint.component.html',
  styleUrls: ['./complaint.component.css']
})
export class ComplaintComponent implements OnInit {

  contactForm : FormGroup;
  submitted = false;
  formload = false;
  formBuilder : FormBuilder;
  public pagedata: any = complaint;

  constructor(formBuilder: FormBuilder,private route: ActivatedRoute, public api: AuthApiService,private subscription: CommonEventsService) { 
    this.formBuilder = formBuilder;
    this.createForm();
  }
  createForm() {
    this.contactForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'email' : [null, Validators.compose([Validators.required, Validators.email])],
      'message' : [null,Validators.compose([Validators.required, Validators.minLength(15)])]
    });
  }
  ngOnInit() {
  }

  onSubmit(myData:any) {
    this.submitted = true;
  
    if (this.contactForm.invalid) {
        return;
   }else{
      this.formload = true;
      this.api.submit("complain",myData).subscribe(result => {
        this.formload = false;
        this.submitted = false;
        this.createForm();
        let sucsess_ob =  {action:'flash_success',redirect_to:'',dataobj:{'message':result.message,'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
      },
      (error) => {this.displayError(error);})
   }
 }
 displayError(error:any) {
    this.formload = false;
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
 }
}
