import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import { Signup } from './validation/signup';  

import { AuthApiService } from './../auth-api.service';  
import { CommonEventsService } from "../../common-events.service";

@Component({
  selector: 'app-parent-sign-up',
  templateUrl: './parent-sign-up.component.html',
  styleUrls: ['./parent-sign-up.component.css']
})
export class ParentSignUpComponent implements OnInit {

  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  signupForm : FormGroup;
  submitted = false;
  formload = false;
  phoOb :any = null;
  country_code : any = "+1";

  constructor(formBuilder: FormBuilder,private route: ActivatedRoute, public api: AuthApiService,private subscription: CommonEventsService) { 
    
    this.signupForm = formBuilder.group({
      'first_name' : [null, Validators.required],
      'last_name': [null, Validators.required],
      'password' : [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      'email' : [null, Validators.compose([Validators.required, Validators.email])],
      'parent_mob_no' : [null, Validators.compose([Validators.required, Validators.maxLength(20)])],
      'confirm_password' : []
    },{'validator': Signup.MatchPassword});
  }

  ngOnInit() {
    
  }
  ngOnDestroy() {
   // this.subscription.unsubscribe();
  }
  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  
  	
  onSubmit(myData:any) {
	this.submitted = true;
	
   if (this.signupForm.invalid) {
        return;
   }else{
	  
      this.formload = true;
      myData.parent_mob_no = myData.parent_mob_no.replace(/[^0-9]/g, '');
      myData.user_type='PU';
	  myData.country_code=this.country_code;
      this.api.submit("parent/register",myData).subscribe(result => {
        this.enableForm();
        let sucsess_ob =  {action:'flash_success',redirect_to:'parent-signin',dataobj:{'message':result.message,'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);
        
      },
      (error) => {this.displayError(error);})
   }
 }
 displayError(error:any) {
  this.enableForm();
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
 }
 enableForm(){
  let that = this;
  setTimeout(function(){
    that.formload = false;
  },2000);
}

  getNumber(obj) {
   // console.log(obj);
  }
  onCountryChange(obj) {
    this.country_code = "+"+obj.dialCode;
  }
  hasError(obj) {
    //console.log(obj);
  }
  telInputObject(obj) {
    this.phoOb = obj;
	//obj.intlTelInput('setCountry', 'pk');
  }
}
