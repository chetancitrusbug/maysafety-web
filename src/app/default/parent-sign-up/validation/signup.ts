import { AbstractControl } from '@angular/forms';
export class Signup {

    static MatchPassword(AC: AbstractControl) {

        let parent_mob_no = AC.get('parent_mob_no').value; // to get value in input tag
        if (parent_mob_no) {
            var number = parent_mob_no.replace(/[^0-9]/g, '');

            if (number.length != 10) {
                AC.get('parent_mob_no').setErrors({ PhoneTen: true })
            }
        }

        let password = AC.get('password').value; // to get value in input tag
        let confirmPassword = AC.get('confirm_password').value; // to get value in input tag
        if (password != confirmPassword) {

            AC.get('confirm_password').setErrors({ MatchPassword: true })
        } else {

            return null
        }
    }
    static PhoneTen(AC: AbstractControl) {
        let parent_mob_no = AC.get('parent_mob_no').value; // to get value in input tag

        if (parent_mob_no) {
            var number = parent_mob_no.replace(/[^0-9]/g, '');

            if (number.length != 10) {
                AC.get('parent_mob_no').setErrors({ PhoneTen: true })
            } else {

                return null
            }
        }
    }

}