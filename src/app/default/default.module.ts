import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TextMaskModule } from 'angular2-text-mask';
import {Ng2TelInputModule} from 'ng2-tel-input';


import { DefaultRoutingModule } from './default-routing.module';



import { HomeComponent } from './home/home.component';
import { CompleteProtectionComponent } from './section/complete-protection/complete-protection.component';
import { FeaturesComponent } from './section/features/features.component';
import { FooterStartFreeTrialComponent } from './section/footer-start-free-trial/footer-start-free-trial.component';
import { HeaderStartFreeTrialComponent } from './section/header-start-free-trial/header-start-free-trial.component';
import { HowItWorkComponent } from './section/how-it-work/how-it-work.component';
import { LatestNewsComponent } from './section/latest-news/latest-news.component';
import { PricingPlansComponent } from './section/pricing-plans/pricing-plans.component';
import { ParentSignUpComponent } from './parent-sign-up/parent-sign-up.component';
import { ParentSignInComponent } from './parent-sign-in/parent-sign-in.component';
import { ChildSignInComponent } from './child-sign-in/child-sign-in.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { SupportComponent } from './support/support.component';
import { PricingpageComponent } from './pricingpage/pricingpage.component';
import { FeaturespageComponent } from './featurespage/featurespage.component';
import { AuthApiService } from '../default/auth-api.service';
import { NonAuthGardService } from '../service/non-auth-gard.service';
import { ChildForgotpassComponentComponent } from './child-forgotpass-component/child-forgotpass-component.component';
import { ComplaintComponent } from './complaint/complaint.component';







@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  DefaultRoutingModule,
  TextMaskModule,
  Ng2TelInputModule
  ],
  declarations: [
    HomeComponent,
    CompleteProtectionComponent,
    FeaturesComponent,
    FooterStartFreeTrialComponent,
    HeaderStartFreeTrialComponent,
    HowItWorkComponent,
    LatestNewsComponent,
    PricingPlansComponent,
    ParentSignUpComponent,
    ParentSignInComponent,
    ChildSignInComponent,
    BlogsComponent,
    BlogDetailComponent,
    AboutComponent,
    ContactComponent,
    ForgotpasswordComponent,
    PrivacyComponent,
    TermsConditionsComponent,
    SupportComponent,
    PricingpageComponent,
    FeaturespageComponent,
    ChildForgotpassComponentComponent,
    ComplaintComponent
  ],
  providers: [AuthApiService, NonAuthGardService]
})
export class DefaultModule { }
