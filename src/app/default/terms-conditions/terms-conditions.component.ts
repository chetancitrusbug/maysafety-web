import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  

import { AuthApiService } from './../auth-api.service';  
import { CommonEventsService } from "../../common-events.service";

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.css']
})
export class TermsConditionsComponent implements OnInit {

  submitted = false;
  formload = false;
  
  public blog: any = {};
  

  constructor(private route: ActivatedRoute, private router: Router,public api: AuthApiService,private subscription: CommonEventsService) { }

  ngOnInit() {
    this.getpageDetail("terms-conditions");
  }
  
  getpageDetail(slug:any) {
        
    this.formload = true;
    this.api.getData("get-page?slug="+slug,[]).subscribe(result => {
      this.formload = false;
      this.blog = result.result;
    },
    (error) => { this.formload = false; })
  }
  displayError(error:any) {
    
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
  }
}