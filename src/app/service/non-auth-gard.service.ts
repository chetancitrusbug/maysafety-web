import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class NonAuthGardService {

  constructor(private route: ActivatedRoute, private router: Router) { 
    
  }
  
  canActivate(): boolean {

    let authuser: any = JSON.parse(localStorage.getItem("authuser"));

    if(authuser && authuser.user_type == 'CU'){
      this.router.navigate(['child']);
      return false;
    }else if(authuser && authuser.user_type == 'PU'){
      this.router.navigate(['parent']);
      return false;
    }else{
      
    }
   
    return true;
  }

}
