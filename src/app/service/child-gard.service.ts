import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class ChildGardService {

  constructor(private route: ActivatedRoute, private router: Router) { 
    
  }
  
  canActivate(): boolean {

    let authuser = JSON.parse(localStorage.getItem("authuser"));
    if(authuser && authuser.user_type == 'CU'){
      
    }else if(authuser && authuser.user_type == 'PU'){
      this.router.navigate(['parent']);
      return false;
    }else{
      this.router.navigate(['child-signin']);
      return false;
    }
   
    return true;
  }

}
