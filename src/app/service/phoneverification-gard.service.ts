import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class PhoneverificationGardService {

  constructor(private route: ActivatedRoute, private router: Router) { 
    
  }
  
  canActivate(): boolean {

    let authuser = JSON.parse(localStorage.getItem("authuser"));
    
    if(authuser && authuser.phone_verified == 1){
      
    }else{
      this.router.navigate(['parent/phone-verification']);
      return false;
    }
   
    return true;
  }

}
