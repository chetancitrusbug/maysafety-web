import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class ParentGardService {

  constructor(private route: ActivatedRoute, private router: Router) { 
    
  }
  
  canActivate(): boolean {

    let authuser = JSON.parse(localStorage.getItem("authuser"));
    if(authuser && authuser.user_type == 'PU'){
      
    }else if(authuser && authuser.user_type == 'CU'){
      this.router.navigate(['child']);
      return false;
    }else{
      this.router.navigate(['parent-signin']);
      return false;
    }
   
    return true;
  }

}
