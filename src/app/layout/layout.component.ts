import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ISubscription } from "rxjs/Subscription";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';


import { CommonEventsService } from "../common-events.service";
import { AuthApiService } from '../default/auth-api.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  private subscription: ISubscription;
  public isHomePage: boolean = true;
  public module: string = 'default';

  public authusername: string = '';
  public notification: any = [];
  public child_notification: any = [];
  public items: any = {};

  constructor(private _scrollToService: ScrollToService, public api: AuthApiService, private route: ActivatedRoute, private router: Router, private jevent: CommonEventsService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.subscription = this.jevent.currentData.subscribe(
      (data: any) => {
        if (data.action == 'flash_success') {
          this.toastr.success(data.dataobj.message, data.dataobj.message_head);
        }
        if (data.action == 'flash_error') {
          this.toastr.error(data.dataobj.message, data.dataobj.message_head);
        }
        if (data.action == 'flash_warning') {
          this.toastr.warning(data.dataobj.message, data.dataobj.message_head);
        }
        if (data.action == 'flash_info') {
          this.toastr.info(data.dataobj.message, data.dataobj.message_head);
        }
        if (data.action == 'load_notification') {
          this.getMyNotification("");
        }
        if (data.action == 'reset_header') {
          this.setCurrentModule();
        }
        if (data.action == 'scroll_top') {
          this.triggerScrollTo();
        }
        if (data.action == 'load_jquery') {
        //  this.loadScript('assets/js/script.js');
        }

        if (data.redirect_to && data.redirect_to != '' && data.redirect_to != 'undefined') {
          this.gotoRoute(data.redirect_to, false);
          this.setCurrentModule();
        }
      });
    this.setCurrentModule();
    this.isHomepage(this.router.url);
    this.loadScript('assets/js/script.js');
    this.getMyNotification("");
  }
  public triggerScrollTo() {

    const config: ScrollToConfigOptions = { target: 'globle_container' };
    this._scrollToService.scrollTo(config);

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getMyNotification(myData: any) {

    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    let authtype = "";
    if (authuser && authuser.user_type == 'CU') {
      authtype = 'child/get_notification?type=Request';
      this.getchildNotification("");
    } else if (authuser && authuser.user_type == 'PU') {
      authtype = 'parent/get_notification';
    }

    if (authtype != "") {
      this.api.getAuthData(authtype, myData).subscribe(result => {
        this.notification = result.result;
      },
        (error) => {
          if (error.error && error.error.status && error.error.status == 401) {
            this.authtokenExpire();
          }
        })
    }
  }
  public getchildNotification(myData: any){
      this.api.getAuthData("child/get_notification?type=Approve", myData).subscribe(result => {
        this.child_notification = result.result;
      },
      (error) => {
        if (error.error && error.error.status && error.error.status == 401) {
          this.authtokenExpire();
        }
      })
  }
  public loadScript(url) {
    //console.log('preparing to load...')
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
  }
  gotoRoute(route: string, isselfpage: boolean) {

    this.isHomepage(route);
    if (this.router.url == "/" && isselfpage) {
      return true;
    } else {
      this.router.navigate(['/' + route]);
    }
    this.triggerScrollTo();

  }
  isHomepage(current_url: string) {
    if (current_url == "/") {
      this.isHomePage = true;
      this.module = 'default';
    } else {
      this.isHomePage = false;
    }
  }

  setCurrentModule() {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));

    if (authuser && authuser.user_type == 'CU') {
      this.module = 'child';
      this.authusername = authuser.first_name + " " + authuser.last_name;
    } else if (authuser && authuser.user_type == 'PU') {
      this.module = 'parent';
      this.authusername = authuser.first_name + " " + authuser.last_name;
    } else {
      this.module = 'default';
      this.authusername = "";
    }
    return true;
  }

  authtokenExpire() {

    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    localStorage.removeItem('authuser');
    this.module = 'default';
    this.authusername = "";
    if (authuser && authuser.user_type == 'CU') {
      this.gotoRoute('child-signin', false);
    } else if (authuser && authuser.user_type == 'PU') {
      this.gotoRoute('parent-signin', false);
    } else {
      this.gotoRoute('/', true);
    }

  }

  doLogout(utype: string) {

    let rdata = "?user_type=" + utype;
    this.api.doLogout("parent/logout" + rdata, []).subscribe(result => {
      localStorage.removeItem('authuser');
      this.toastr.success("Logout successfully .", "Success !");
      this.gotoRoute('/', true);
      this.loadScript('assets/js/script.js');

    },
      (error) => {
        localStorage.removeItem('authuser');
        this.toastr.success("Logout successfully .", "Success !");
        this.gotoRoute('/', true);
        this.loadScript('assets/js/script.js');
      })


  }



}
